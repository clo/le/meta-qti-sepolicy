# Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/usr/bin/QCMAP_ConnectionManager  --  gen_context(system_u:object_r:qcmap_cm_exec_t,s0)
/usr/bin/QCMAP_Bootup             --  gen_context(system_u:object_r:qcmap_cm_exec_t,s0)
/etc/data(/.*)?                       gen_context(system_u:object_r:qcmap_etc_data_t,s0)
/systemrw/data(/.*)?                  gen_context(system_u:object_r:qcmap_etc_data_t,s0)
/systemrw/rt_tables(/.*)?             gen_context(system_u:object_r:qcmap_etc_data_t,s0)
/systemrw/misc/wifi(/.*)?             gen_context(system_u:object_r:qcmap_etc_data_t,s0)

/data/data_qcmap(/.*)?                gen_context(system_u:object_r:qcmap_data_t,s0)
/data/entropy_file                --  gen_context(system_u:object_r:qcmap_data_t,s0)
/data/entropy_file1               --  gen_context(system_u:object_r:qcmap_data_t,s0)

/usr/bin/dnsmasq                  --  gen_context(system_u:object_r:dnsmasq_exec_t,s0)

/data/lighttpd\.conf              --  gen_context(system_u:object_r:httpd_data_t,s0)
/data/mdm9625\.com\.crt           --  gen_context(system_u:object_r:httpd_data_t,s0)
/data/mdm9625\.com\.key           --  gen_context(system_u:object_r:httpd_data_t,s0)
/data/mdm9625\.com\.pem           --  gen_context(system_u:object_r:httpd_data_t,s0)
/data/nopwd\.mdm9625\.com\.key    --  gen_context(system_u:object_r:httpd_data_t,s0)
/data/openssl\.cnf                --  gen_context(system_u:object_r:httpd_data_t,s0)
/data/www(/.*)?                       gen_context(system_u:object_r:httpd_data_t,s0)
#/WEBSERVER(/.*)?                      gen_context(system_u:object_r:httpd_user_content_t,s0)
/run/resolv\.conf                 --  gen_context(system_u:object_r:net_conf_t,s0)
/run/lock(/.*)                        gen_context(system_u:object_r:pppd_run_lock_t,s0)
/dev/gsb                         -c   gen_context(system_u:object_r:datadpo_device_t,s0)
/var/run/data(/.*)?                   gen_context(system_u:object_r:qcmap_var_run_t,s0)
/run/data(/.*)?                       gen_context(system_u:object_r:qcmap_var_run_t,s0)
/tmp/data(/.*)?                       gen_context(system_u:object_r:qcmap_tmp_t,s0)

#WLAN scripts
/etc/initscripts/start_qcmap_wlan_le              --    gen_context(system_u:object_r:wlan_initrc_exec_t,s0)
/etc/initscripts/start_qcmap_wpa_supplicant_le    --    gen_context(system_u:object_r:NetworkManager_exec_t,s0)
/etc/initscripts/avahi.*                          --    gen_context(system_u:object_r:avahi_initrc_exec_t,s0)

#WLAN SDx24 AUTO
/usr/lib/systemd/system/init_qti_wlan_auto\.service --  gen_context(system_u:object_r:wlan_initrc_exec_t,s0)


#Data path opt
/usr/bin/data_path_opt          --    gen_context(system_u:object_r:data_path_opt_exec_t,s0)
/usr/sbin/minidlnad             --    gen_context(system_u:object_r:minidlna_exec_t,s0)

#TinyProxy
/usr/sbin/tinyproxy             --    gen_context(system_u:object_r:tinyproxy_exec_t,s0)
/etc/tinyproxy(/.*)?                  gen_context(system_u:object_r:tinyproxy_etc_t,s0)
/var/log/tinyproxy(/.*)?              gen_context(system_u:object_r:tinyproxy_log_t,s0)
/var/run/tinyproxy(/.*)?              gen_context(system_u:object_r:tinyproxy_var_run_t,s0)

#miniupnp
/usr/sbin/miniupnpd             --    gen_context(system_u:object_r:miniupnp_exec_t,s0)

#load kernel modules
/usr/lib/systemd/system/qcmap_load_module@.service -- gen_context(system_u:object_r:qcmap_kernel_module_t,s0)

#SSDK
/dev/switch_ssdk                  -c   gen_context(system_u:object_r:ssdk_device_t,s0)
