# Copyright (c) 2016,2018-2020 The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

policy_module(location, 1.0)

gen_require(`
    type system_dir_t;
    type adbd_t;
    type systemd_resolved_var_run_t;
    type default_t;
    type dhcpc_var_run_t;
    type qcmap_var_run_t;
    type node_t;
    type tmpfs_t;
    type sysctl_t;
    type netmgrd_socket_device_t;
')

########################################
#
# Declarations
#

# location conf files
type loc_etc_t;
files_type(loc_etc_t)

#location daemon
type loc_t;
type loc_exec_t;

init_vendor_domain(loc_t, loc_exec_t)
inittab_domtrans(loc_t, loc_exec_t)

corecmd_exec_bin(loc_t)
dev_read_sysfs(loc_t)
dev_read_urand(loc_t)
files_read_etc_files(loc_t)
# read location conf files
files_read_loc_etc_files(loc_t)
logging_send_syslog_msg(loc_t)
leprop_rw_props(loc_t)
allow loc_t self:{ socket udp_socket } create_socket_perms;
allow loc_t self:process { setcap signal };
allow loc_t self:netlink_route_socket read;
allow loc_t xtra_daemon_t:process signal;
allow loc_t slim_daemon_t:process sigkill;

# Drop from root
allow loc_t self:capability { setuid setgid };
kernel_read_network_state(loc_t)

type loc_data_t;
user_data_files_type(loc_data_t)
data_filetrans(loc_t, loc_data_t, { file dir })
manage_user_data_files(loc_t, loc_data_t)
gen_require(`
    type systemd_tmpfiles_t;
')
allow systemd_tmpfiles_t loc_data_t:dir { relabelfrom relabelto };
allow systemd_tmpfiles_t loc_data_t:file { relabelfrom relabelto };

# location init script for sysvinit systems
ifndef(`init_systemd', `
    type loc_initrc_exec_t;
    type loc_initrc_t;

    init_script_domain(loc_initrc_t, loc_initrc_exec_t)
    domtrans_pattern(loc_initrc_t, loc_exec_t, loc_t)
    corecmd_exec_shell(loc_initrc_t)
    corecmd_exec_bin(loc_initrc_t)
    data_filetrans(loc_initrc_t, loc_data_t, { file dir })
    files_read_etc_files(loc_initrc_t)
    dev_write_kmsg(loc_initrc_t)
    manage_user_data_files(loc_initrc_t, loc_data_t)
    read_user_data_files(loc_initrc_t, loc_data_t);
    allow loc_initrc_t data_t:dir { setattr };
    allow loc_initrc_t loc_data_t:file { getattr setattr };
    allow loc_initrc_t self:capability { chown fowner fsetid dac_override };
    allow loc_initrc_t socket_device_t:dir { create setattr };

    dontaudit loc_initrc_t console_device_t:chr_file read;
')

### location hal

type loc_hald_t;
type loc_hald_exec_t;

init_vendor_domain(loc_hald_t, loc_hald_exec_t)
inittab_domtrans(loc_hald_t, loc_hald_exec_t)
logging_send_syslog_msg(loc_hald_t)
sysnet_read_config(loc_hald_t)

userdebug_or_eng(`
    rw_diag_dev(loc_hald_t)
')
dev_read_sysfs(loc_hald_t)
files_read_etc_files(loc_hald_t)
# read location conf files
files_read_loc_etc_files(loc_hald_t)
files_rw_qcmap_etc_data_files(loc_hald_t)
leprop_rw_props(loc_hald_t)
dev_read_realtime_clock(loc_hald_t)
device_rw_socket_files(loc_hald_t, netmgrd_socket_device_t)

allow loc_hald_t tmpfs_t:filesystem getattr;
allow loc_hald_t tmpfs_t:dir search;
allow loc_hald_t sysctl_t:dir search;
allow loc_hald_t self:capability { net_admin setgid setuid net_bind_service};
allow loc_hald_t self:process setcap;
allow loc_hald_t self:{ socket udp_socket unix_dgram_socket } create_socket_perms;
allow loc_hald_t node_t:udp_socket node_bind;
allow loc_hald_t unreserved_port_t:udp_socket name_bind;
allow loc_hald_t self:netlink_socket create_socket_perms;
allow loc_hald_t { loc_t engine_service_t }:unix_dgram_socket sendto;
allow loc_t loc_hald_t:unix_dgram_socket sendto;
allow loc_hald_t self:unix_dgram_socket sendto;
allow loc_hald_t xtra_daemon_t:unix_dgram_socket sendto;
allow loc_hald_t adbd_t:unix_dgram_socket sendto;
allow loc_hald_t socket_device_t:sock_file { write unlink };
allow loc_hald_t self:process signal;
dev_read_urand(loc_hald_t);

# boot kpi
userdebug_or_eng(`
    gen_require(`
        type debugfs_t;
    ')
    allow loc_hald_t debugfs_t:dir search;
    allow loc_hald_t debugfs_t:file { write open };
    corecmd_exec_shell(loc_hald_t);
')

# temporary fix till /target changes are made cr#2387138
read_files_pattern(loc_hald_t, default_t, default_t);
data_filetrans(loc_hald_t, loc_data_t, { file dir })
manage_user_data_files(loc_hald_t, loc_data_t)
qnetmgr_use_unix_stream_sockets(loc_hald_t)
qnetmgr_write_data_sock_files(loc_hald_t)

search_user_data_dir(loc_hald_t)
userdebug_or_eng(`
    gen_require(`
        type unconfined_t;
        class service { start status stop };
    ')
    allow unconfined_t loc_hald_exec_t:service { start status stop };
    allow unconfined_t loc_exec_t:service { start status stop };
')

dontaudit loc_hald_t self:capability { dac_override dac_read_search };

### xtwifi_agent

type xtwifi_agent_t;
type xtwifi_agent_exec_t;

init_vendor_domain(xtwifi_agent_t, xtwifi_agent_exec_t)
# Launched by loc_launcher instead of init
domtrans_pattern(loc_t, xtwifi_agent_exec_t, xtwifi_agent_t)
logging_send_syslog_msg(xtwifi_agent_t)

files_read_etc_files(xtwifi_agent_t)
# read location conf files
files_read_loc_etc_files(xtwifi_agent_t)
allow xtwifi_agent_t self:{ udp_socket unix_dgram_socket } create_socket_perms;
allow xtwifi_agent_t proc_t:file read_file_perms;

data_filetrans(xtwifi_agent_t, loc_data_t, { file dir })
manage_user_data_files(xtwifi_agent_t, loc_data_t)

### xtwifi_client

type xtwifi_client_t;
type xtwifi_client_exec_t;

init_vendor_domain(xtwifi_client_t, xtwifi_client_exec_t)
# Launched by loc_launcher instead of init
domtrans_pattern(loc_t, xtwifi_client_exec_t, xtwifi_client_t)
logging_send_syslog_msg(xtwifi_client_t)

files_read_etc_files(xtwifi_client_t)
# read location conf files
files_read_loc_etc_files(xtwifi_client_t)
dev_read_sysfs(xtwifi_client_t)
leprop_rw_props(xtwifi_client_t)

allow xtwifi_client_t self:{ socket unix_dgram_socket } create_socket_perms;

data_filetrans(xtwifi_client_t, loc_data_t, { file dir })
manage_user_data_files(xtwifi_client_t, loc_data_t)
dev_read_urand(xtwifi_client_t)

### engine-service
type engine_service_t;
type engine_service_exec_t;
gen_require(`
    type unreserved_port_t;
')

init_vendor_domain(engine_service_t, engine_service_exec_t)
# Launched by loc_launcher instead of init
domtrans_pattern(loc_t, engine_service_exec_t, engine_service_t)
logging_send_syslog_msg(engine_service_t)

userdebug_or_eng(`
    rw_diag_dev(engine_service_t)
')

allow engine_service_t self:udp_socket create_socket_perms;
allow engine_service_t loc_hald_t:unix_dgram_socket sendto;
allow engine_service_t self:socket create_socket_perms;
sysnet_read_config(engine_service_t);
allow engine_service_t self:tcp_socket create_socket_perms;
allow engine_service_t unreserved_port_t:tcp_socket name_connect;
read_files_pattern(engine_service_t, systemd_resolved_var_run_t, systemd_resolved_var_run_t)

dev_read_urand(engine_service_t)
dev_read_sysfs(engine_service_t)
files_read_etc_files(engine_service_t)
# read location conf files
files_read_loc_etc_files(engine_service_t)
data_filetrans(engine_service_t, loc_data_t, { file dir })
manage_user_data_files(engine_service_t, loc_data_t)
## Read virtual memory overcommit sysctl - /proc/sys/vm/overcommit_memory
kernel_read_vm_overcommit_sysctl(engine_service_t)

### xtra-daemon
type xtra_daemon_t;
type xtra_daemon_exec_t;
init_vendor_domain(xtra_daemon_t, xtra_daemon_exec_t)

# Launched by loc_launcher instead of init
domtrans_pattern(loc_t, xtra_daemon_exec_t, xtra_daemon_t)
logging_send_syslog_msg(xtra_daemon_t)

allow xtra_daemon_t dhcpc_var_run_t:file { read open getattr };
allow xtra_daemon_t loc_hald_t:unix_dgram_socket sendto;
allow xtra_daemon_t self:{ socket udp_socket } create_socket_perms;
allow xtra_daemon_t self:tcp_socket { read write create connect getattr setopt name_connect getopt };
# read build.prop
leprop_read_props(xtra_daemon_t)
## this is for access to /run/resolv.conf
files_read_generic_pids(xtra_daemon_t)
allow xtra_daemon_t self:process signal;
allow xtra_daemon_t socket_device_t:sock_file write;
read_files_pattern(xtra_daemon_t, init_var_run_t, init_var_run_t);
read_files_pattern(xtra_daemon_t, systemd_resolved_var_run_t, systemd_resolved_var_run_t);

# temporary fix till /target changes are made cr#2387138
read_files_pattern(xtra_daemon_t, default_t, default_t);

corenet_tcp_connect_http_port(xtra_daemon_t)
sysnet_read_config(xtra_daemon_t)
dev_read_urand(xtra_daemon_t)
dev_read_rand(xtra_daemon_t)
miscfiles_read_generic_certs(xtra_daemon_t)
dev_read_sysfs(xtra_daemon_t)
leprop_rw_props(xtra_daemon_t)
kernel_read_vm_overcommit_sysctl(xtra_daemon_t)

##access qdma socket
rw_vendor_qdma_sock_file(xtra_daemon_t);

type loc_tmp_t;
files_tmp_file(loc_tmp_t)
files_tmp_filetrans(xtra_daemon_t, loc_tmp_t, file)
allow xtra_daemon_t loc_tmp_t:file { create_file_perms write_file_perms read_file_perms };
files_read_generic_tmp_symlinks(xtra_daemon_t)

files_read_etc_files(xtra_daemon_t)
# read location conf files
files_read_loc_etc_files(xtra_daemon_t)
data_filetrans(xtra_daemon_t, loc_data_t, { file dir })
manage_user_data_files(xtra_daemon_t, loc_data_t)
allow xtra_daemon_t self:netlink_route_socket { create_socket_perms nlmsg_read };
## read resolv.conf
read_files_pattern(xtra_daemon_t, qcmap_var_run_t, qcmap_var_run_t);

userdebug_or_eng(`
    allow xtra_daemon_t adbd_t:unix_dgram_socket sendto;
')

### lowi-server
type lowi_server_t;
type lowi_server_exec_t;
init_vendor_domain(lowi_server_t, lowi_server_exec_t)

# Launched by loc_launcher instead of init
domtrans_pattern(loc_t, lowi_server_exec_t, lowi_server_t)
logging_send_syslog_msg(lowi_server_t)

data_filetrans(lowi_server_t, loc_data_t, { file dir })
manage_user_data_files(lowi_server_t, loc_data_t)

kernel_read_network_state(lowi_server_t)
allow lowi_server_t self:{ netlink_route_socket udp_socket netlink_generic_socket } create_socket_perms;
allow lowi_server_t self:capability net_admin;

files_read_etc_files(lowi_server_t)
# read location conf files
files_read_loc_etc_files(lowi_server_t)

kernel_dontaudit_request_load_module(lowi_server_t)

### slim_daemon
type slim_daemon_t;
type slim_daemon_exec_t;
init_vendor_domain(slim_daemon_t, slim_daemon_exec_t)

# Launched by loc_launcher instead of init
domtrans_pattern(loc_t, slim_daemon_exec_t, slim_daemon_t)
logging_send_syslog_msg(slim_daemon_t)

files_read_etc_files(slim_daemon_t)
# read location conf files
files_read_loc_etc_files(slim_daemon_t)
manage_user_data_files(slim_daemon_t, loc_data_t)
data_filetrans(slim_daemon_t, loc_data_t, { file dir })
read_files_pattern(slim_daemon_t, default_t, default_t)

userdebug_or_eng(`
    rw_diag_dev(slim_daemon_t)
')

allow slim_daemon_t self:capability { setuid setgid net_bind_service };
allow slim_daemon_t self:process { setcap getcap };
allow slim_daemon_t self:socket create_socket_perms;
# read, write /sys/devices/platform/*/i2c*/iio:device*/*, read lnk file
dev_rw_sysfs(slim_daemon_t)
# Read input event devices - /dev/input/event*
dev_read_input(slim_daemon_t)
## read /proc/bus/input/devices
allow slim_daemon_t proc_t:file read_file_perms;
## read char device files /dev/iio:deviceXX
device_read_iio_device(slim_daemon_t)
## net_raw capability access required for QMI communication.
allow slim_daemon_t self:capability net_raw;
## Read virtual memory overcommit sysctl - /proc/sys/vm/overcommit_memory
kernel_read_vm_overcommit_sysctl(slim_daemon_t)

### location socket
type loc_socket_t;
files_type(loc_socket_t)

device_manage_socket_file({ loc_t loc_hald_t xtra_daemon_t engine_service_t }, loc_socket_t)
ifndef(`init_systemd', `device_manage_socket_file(loc_initrc_t, loc_socket_t)')

allow loc_socket_t device_t:filesystem associate;
allow { loc_t loc_hald_t xtra_daemon_t engine_service_t } loc_socket_t:dir manage_dir_perms;
allow { loc_t loc_hald_t xtwifi_client_t xtra_daemon_t lowi_server_t engine_service_t slim_daemon_t } self:fifo_file rw_fifo_file_perms;

allow { xtwifi_agent_t xtwifi_client_t lowi_server_t xtra_daemon_t engine_service_t slim_daemon_t } loc_socket_t:sock_file write;
allow { xtwifi_agent_t xtwifi_client_t xtra_daemon_t lowi_server_t engine_service_t slim_daemon_t } loc_socket_t:dir search_dir_perms;
allow { lowi_server_t xtwifi_agent_t xtwifi_client_t xtra_daemon_t engine_service_t slim_daemon_t loc_hald_t } loc_t:unix_stream_socket { connectto rw_stream_socket_perms };
device_search_sockets({ xtwifi_agent_t xtwifi_client_t xtra_daemon_t lowi_server_t engine_service_t slim_daemon_t}, loc_socket_t)

## common rules
dontaudit {loc_t loc_hald_t xtwifi_agent_t xtwifi_client_t lowi_server_t xtra_daemon_t engine_service_t slim_daemon_t} system_dir_t:dir search;
dontaudit {loc_t loc_hald_t xtwifi_agent_t xtwifi_client_t lowi_server_t xtra_daemon_t engine_service_t slim_daemon_t} default_t:dir search;
