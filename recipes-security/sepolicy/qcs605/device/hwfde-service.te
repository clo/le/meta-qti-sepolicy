# Copyright (c) 2018, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

policy_module(hwfde-service, 1.0)

########################################
#
# Declarations
#

type hwfde_service_t;
type hwfde_service_exec_t;

require {
    type proc_t;
}

init_daemon_domain(hwfde_service_t, hwfde_service_exec_t)
logging_send_syslog_msg(hwfde_service_t)

# allow hwfde_service to do basic operation with qseecom driver
dev_rw_generic_chr_files(hwfde_service_t)

# allow hwfde_service to read metadata from media-encryption.conf file
type hwfde_conf_file_t;
allow hwfde_service_t hwfde_conf_file_t:file { open read };

# allow hwfde_service to get metadata from footer partition
type footer_blk_t;
dev_node(footer_blk_t);
allow hwfde_service_t footer_blk_t:blk_file { open read write };

# allow hwfde_service to encrypt userdata partition
storage_raw_write_removable_device(hwfde_service_t)
storage_raw_read_removable_device(hwfde_service_t)

# allow hwfde_service to read commandline from /proc/cmdline
kernel_getattr_proc(hwfde_service_t)
allow hwfde_service_t proc_t:file { open read };

# allow hwfde_service to mount userata partition on /data
fs_mount_xattr_fs(hwfde_service_t)
data_mount_user(hwfde_service_t)

# allow hwfde_service to read/write leproperties
leprop_rw_props(hwfde_service_t)

files_polyinstantiate_all(hwfde_service_t)
