# Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

policy_module(ipc-webserver, 1.0)

gen_require(`
    type system_dir_t;
    type qmmf_config_file_t;
')

########################################
#
# Declarations
#

type ipc_webserver_t;
type ipc_webserver_exec_t;

allow ipc_webserver_t system_dir_t:dir search_dir_perms;
read_files_pattern(ipc_webserver_t, qmmf_config_file_t, qmmf_config_file_t)

gen_require(`
 type firmware_t;
 type sysctl_t;
 type sysctl_net_t;
 type node_t;
 type devlog_t;
 type syslogd_t;
 type urandom_device_t;
 type unreserved_port_t;
')

init_vendor_domain(ipc_webserver_t, ipc_webserver_exec_t)
servicemanager_binder_use(ipc_webserver_t)
manage_qmmf_data_files(ipc_webserver_t)

#self
allow ipc_webserver_t self:process getsched;
allow ipc_webserver_t self:fifo_file { write read getattr };
allow ipc_webserver_t self:capability net_raw;
allow ipc_webserver_t self:socket { create getattr read write };
allow ipc_webserver_t self:unix_dgram_socket { create_socket_perms };
allow ipc_webserver_t self:tcp_socket { create_socket_perms listen accept node_bind name_bind };
allow ipc_webserver_t self:udp_socket { create ioctl };
allow ipc_webserver_t syslogd_t:unix_dgram_socket sendto;

allow ipc_webserver_t sysctl_t:dir search;
allow ipc_webserver_t sysctl_net_t:dir search;
allow ipc_webserver_t sysctl_net_t:file { read open };

allow ipc_webserver_t firmware_t:dir { search };
allow ipc_webserver_t firmware_t:file { open getattr read};

allow ipc_webserver_t node_t:tcp_socket node_bind;
allow ipc_webserver_t unreserved_port_t:tcp_socket {name_bind name_connect };
allow ipc_webserver_t devlog_t:sock_file write;
allow ipc_webserver_t urandom_device_t:chr_file read;

userdebug_or_eng(`
    gen_require(`
        type default_t;
        type diag_device_t;
    ')

    rw_chr_files_pattern(ipc_webserver_t, device_t, diag_device_t);

    allow ipc_webserver_t default_t:dir { search };
    allow ipc_webserver_t default_t:file { read getattr open };
')

